import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class MessageConsumer {

    private static final String TOPIC_NAME = "in-topic";

    public static void main(String[] args) {

        Properties consumerProperties = ConsConnect.connect();
        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(consumerProperties);
        kafkaConsumer.subscribe(Collections.singletonList(TOPIC_NAME));

        try {
            while (true) {
                ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(1000));
                for (ConsumerRecord<String, String> record : records) {
                    String corrID = record.key();
                    String msg = record.value();
                    Thread t1 = new Thread(() -> {
                        MessageProducer messageProducer = new MessageProducer(msg, corrID);
                        messageProducer.run();
                    });
                    t1.start();
                    kafkaConsumer.commitAsync();
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            kafkaConsumer.close();
        }
    }
}
