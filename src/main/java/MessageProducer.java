import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Properties;

class MessageProducer {

    private static final String TOPIC_NAME = "out-topic";
    private String msg;
    private String corrID;

    MessageProducer(String msg, String corrID) {
        this.msg = msg;
        this.corrID = corrID;
    }

    void run() {

        Properties producerProperties = ProdConnect.connect();
        ByteArrayInputStream input = null;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            input = new ByteArrayInputStream(msg.getBytes(StandardCharsets.UTF_8));
            Document doc = Objects.requireNonNull(builder).parse(input);
            NodeList nodeList = Objects.requireNonNull(doc).getElementsByTagName("guid");
            String guid = nodeList.item(0).getTextContent();
            String xmlMessage = "<testRs>\n" +
                    "\t<status> OK </status>\n" +
                    "\t<guid>" + " " + guid + " " + "</guid>\n" +
                    "</testRs>";
            try (Producer<String, String> producer = new KafkaProducer<>(producerProperties)) {
                ProducerRecord<String, String> record
                        = new ProducerRecord<>(TOPIC_NAME, corrID, xmlMessage);
                producer.send(record);
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert input != null;
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
